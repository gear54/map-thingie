import { defineStore } from 'pinia';

type HistoryAction = {
  title: string; 
  undo: () => void;
  redo: () => void;
};

const useHistoryStore = defineStore('history', () => {
  const actions = ref<HistoryAction[]>([]);
  const lastActionPointer = ref(-1);

  const hasUndo = computed(() => lastActionPointer.value > -1 && actions.value.length > 0);
  const undoTitle = computed(() => hasUndo.value ? actions.value[lastActionPointer.value].title : undefined);

  const hasRedo = computed(() => lastActionPointer.value < actions.value.length - 1);
  const redoTitle = computed(() => hasRedo.value ? actions.value[lastActionPointer.value + 1].title : undefined);

  const applyAction = (action: HistoryAction) => {
    lastActionPointer.value += 1;
    actions.value = [...actions.value.slice(0, lastActionPointer.value), action];
    action.redo(); // Actually perform action
  };

  const undo = () => {
    if (!hasUndo.value) {
      throw new Error('Can\'t undo');
    }

    actions.value[lastActionPointer.value].undo();
    lastActionPointer.value -= 1;
  };

  const redo = () => {
    if (!hasRedo.value) {
      throw new Error('Can\'t redo');
    }

    actions.value[lastActionPointer.value + 1].redo(); // In case it throw we don't want to move pointer
    lastActionPointer.value += 1;
  };

  return {
    applyAction,

    hasUndo,
    undoTitle,

    hasRedo,
    redoTitle,

    undo,
    redo,

    // Private
    actions,
    lastActionPointer,
  };
});

export default useHistoryStore;
