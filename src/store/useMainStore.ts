import { defineStore, storeToRefs } from 'pinia';

import useHistoryStore from './useHistoryStore';

import { type Marker, markerDefaults } from '@/helpers/markers';
import type { Coordinates } from '@/helpers/geometry';

const useMainStore = defineStore('main', () => {
  const historyStore = useHistoryStore();
  const { applyAction, undo, redo } = historyStore;
  const { undoTitle, redoTitle } = storeToRefs(historyStore);

  const markers = ref<Marker[]>([]);

  const addMarkerAction = (coordinates: Coordinates) => {
    const marker = {
      /**
       * Even though coordinates + type should be pretty unique, we can
       * exclude any possibility of error if we use ms-based ID for key.
       */
      id: Date.now(),
  
      type: 'text',
      coordinates,
      text: markerDefaults.text,
    } as const;

    const undo = () => {
      markers.value = markers.value.filter(({ id }) => id !== marker.id);
    };

    const redo = () => {
      markers.value.push(marker);
    };

    applyAction({ title: 'add marker', undo, redo });

    return marker;
  }

  const removeMarkerAction = (markerId: Marker['id']) => {
    const marker = markers.value.find(({ id }) => id === markerId);

    if (!marker) {
      throw new Error(`Markerk ID not found: ${markerId}`);
    }

    const undo = () => {
      markers.value.push(marker);
    };

    const redo = () => {
      markers.value = markers.value.filter(({ id }) => id !== markerId);
    };
    
    applyAction({ title: 'remove marker', undo, redo });
  };

  return {
    // History
    undoTitle,
    redoTitle,

    undo,
    redo,
    
    // Business logic
    markers, 
    
    addMarkerAction,
    removeMarkerAction,
  };
});

export default useMainStore;
