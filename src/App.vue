<script setup lang="ts">
import { storeToRefs } from 'pinia';

import MapView from '@/components/MapView.vue';
import MainControls, { type ControlModel } from '@/components/MainControls.vue';
import TextMarker from '@/components/TextMarker.vue';
import IconMarker from '@/components/IconMarker.vue';
import EditMarkerForm from '@/components/EditMarkerForm.vue';
import ExportFrame, { type ExportFrameExposed } from '@/components/ExportFrame.vue';

import downloadDataUrl from '@/helpers/downloadDataUrl';
import type { Coordinates } from '@/helpers/geometry';
import { type Marker, markerDefaults } from '@/helpers/markers';

import useWindowSize from '@/helpers/useWindowSize';

import useMainStore from '@/store/useMainStore';

const accessToken: string = import.meta.env.VITE_MAPBOX_TOKEN;

const mainStore = useMainStore();
const { addMarkerAction, removeMarkerAction } = mainStore;
const { markers } = storeToRefs(mainStore);

const controlsModel = ref<ControlModel>({
  layers: ['poi-labels', 'elevation-shading'],
  exportFrameSize: { height: 0, width: 0 },
  isAddingMarker: false,
});

const selectedMarkerId = ref<Marker['id'] | undefined>();
const exportFrame = ref<ExportFrameExposed | undefined>();

const selectedMarker = computed({
  get: () => markers.value.find(({ id }) => id === selectedMarkerId.value),

  set: (value) => {
    const index = markers.value.findIndex(({ id }) => id === selectedMarkerId.value);

    if (index === -1) {
      throw new Error('Failed to find selected marker in markers array');
    }

    if (!value) {
      return;
    }

    Object.assign(markers.value[index], value);
  },
});

const exportFrameBorderWidth = 1;
const maxExportFrameSize = useWindowSize(exportFrameBorderWidth);

onMounted(() => {
  controlsModel.value.exportFrameSize = {
    height: window.innerHeight / 2,
    width: window.innerWidth / 2,
  };
});

const isMarkerSelected = (marker: Marker) => {
  return selectedMarkerId.value === marker.id;
};

const selectMarker = (marker: Marker) => {
  selectedMarkerId.value = marker.id;
};

const unselectMarker = () => {
  selectedMarkerId.value = undefined;
};

const removeMarker = () => {
  if (selectedMarkerId.value) {
    removeMarkerAction(selectedMarkerId.value);
    unselectMarker();
  }
};

const cancelAction = () => {
  if (controlsModel.value.isAddingMarker) {
    controlsModel.value.isAddingMarker = false;

    return;
  }

  if (selectedMarkerId.value) {
    unselectMarker();

    return;
  }
};

const keypressHandler = ({ key }: KeyboardEvent) => {
  if (key === 'Escape') {
    cancelAction();
  }

  if (key === 'Delete') {
    removeMarker();
  }
};

/** Add marker if we're adding marker. Also unselect marker if selected. */
const mapClickHandler = (coordinates: Coordinates) => {
  unselectMarker();

  if (!controlsModel.value.isAddingMarker) {
    return;
  }

  controlsModel.value.isAddingMarker = false;

  const marker = addMarkerAction(coordinates);

  selectedMarkerId.value = marker.id;
};

// Need this to not show UI on exported screenshots
const isExporting = ref(false);

/**
 * We could make our 'export' button be a link with current data URL (since
 * semantically it's what it is - a download link) but we'd have to attach so
 * many different event handlers for updating it all over the place that it would
 * probably impact performance and definitely impact code simplicity.
 *
 * Would be a good flex tho :D
 */
const exportMap = async () => {
  if (!exportFrame.value) {
    throw new Error('Failed to find export frame');
  }

  isExporting.value = true;
  await nextTick();

  const dataUrl = await exportFrame.value.exportAsDataUrl();

  isExporting.value = false;

  downloadDataUrl(`map-thingie-${Date.now()}`, dataUrl);
};
</script>

<template>
  <main :class="$style.app" @keydown="keypressHandler">
    <MapView
      :access-token="accessToken"
      :layers="controlsModel.layers"
      :is-targeting="controlsModel.isAddingMarker"
      @click="mapClickHandler"
    >
      <template #controls-left>
        <!-- v-show won't work here because popups are attached to body -->
        <MainControls
          v-if="!isExporting"
          v-model="controlsModel"
          :max-export-frame-size="maxExportFrameSize"
          :undoTitle="mainStore.undoTitle"
          :redoTitle="mainStore.redoTitle"
          @undo="mainStore.undo"
          @redo="mainStore.redo"
          @click="unselectMarker"
          @export="exportMap"
        />
      </template>

      <template #controls-right>
        <!-- Stop keydown events so that pressing delete in forms won't remove marker -->
        <EditMarkerForm
          v-if="selectedMarker"
          v-show="!isExporting"
          v-model="selectedMarker"
          :defaults="markerDefaults"
          @remove="removeMarker"
          @keydown.stop=""
        />
      </template>

      <template #markers>
        <template v-for="marker in markers" :key="marker.id">
          <TextMarker
            v-if="marker.type === 'text'"
            :coordinates="marker.coordinates"
            :text="marker.text"
            :is-selected="!isExporting && isMarkerSelected(marker)"
            @click="selectMarker(marker)"
            @move="marker.coordinates = $event"
          />

          <IconMarker
            v-if="marker.type === 'icon'"
            :coordinates="marker.coordinates"
            :icon="marker.icon"
            :is-selected="!isExporting && isMarkerSelected(marker)"
            @click="selectMarker(marker)"
            @move="marker.coordinates = $event"
          />
        </template>
      </template>
    </MapView>
    <ExportFrame
      ref="exportFrame"
      :class="$style['export-frame']"
      :border-width="exportFrameBorderWidth"
      :height="controlsModel.exportFrameSize.height"
      :width="controlsModel.exportFrameSize.width"
    />
  </main>
</template>

<style module lang="scss">
.app {
  position: relative;
  height: 100%;
}
.export-frame {
  position: absolute;
  z-index: var(--z-index-export-frame);
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  pointer-events: none;
}
</style>

<style lang="scss">
:root {
  --spacing: 8px;
  --z-index-map-controls: 1000;
  --z-index-export-frame: 100;
}

html,
body,
#app {
  height: 100%;
}

body {
  font-family: 'Helvetica Neue', Helvetica, 'PingFang SC', 'Hiragino Sans GB', 'Microsoft YaHei', '微软雅黑', Arial,
    sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  margin: 0;
}
</style>
@/helpers/useMainStore