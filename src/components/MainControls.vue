<script setup lang="ts">
import { ElForm, ElFormItem, ElButton, ElTooltip, ElCheckbox, ElCheckboxGroup, ElInputNumber, ElDropdown } from 'element-plus';

import {
  Files as FilesIcon,
  Picture as PictureIcon,
  MapLocation as MapLocationIcon,
  ArrowLeftBold as ArrowLeftIcon,
  ArrowRightBold as ArrowRightIcon,
} from '@element-plus/icons-vue';

import type { Size } from '@/helpers/geometry';
import { type Layer } from '@/helpers/layers';

export type ControlModel = {
  layers: Layer[];
  exportFrameSize: Size;
  isAddingMarker: boolean;
};

const layerItems: { name: string; value: Layer }[] = [
  { name: 'Points of interest', value: 'poi-labels' },
  { name: 'Elevation shading', value: 'elevation-shading' },
];

const dropdownProps = {
  placement: 'right',
  trigger: 'click',
  'popper-options': { modifiers: [{ name: 'preventOverflow', options: { padding: 8 } }] },
} as const;

// This basically copies the object to prevent reactivity problems
const getFormModel = ({ exportFrameSize, ...rest }: ControlModel): ControlModel => {
  return {
    ...rest,

    exportFrameSize: { height: exportFrameSize.height, width: exportFrameSize.width },
  };
};

const props = withDefaults(
  defineProps<{
    modelValue: ControlModel;
    maxExportFrameSize?: Size;
    redoTitle?: string;
    undoTitle?: string;
  }>(),
  { maxExportFrameSize: () => ({ height: 1, width: 1 }) },
);

const emit = defineEmits<{
  (event: 'update:modelValue', value: ControlModel): void;
  (event: 'export'): void;
  (event: 'undo'): void;
  (event: 'redo'): void;
}>();

const clampSize = (size: Size, maxSize: Size) => {
  return {
    height: Math.min(maxSize.height, size.height),
    width: Math.min(maxSize.width, size.width),
  };
};

watch(
  () => props.maxExportFrameSize,

  (maxSize) => {
    const currentSize = props.modelValue.exportFrameSize;

    formModel.value = getFormModel({
      ...props.modelValue,

      exportFrameSize: clampSize(currentSize, maxSize),
    });
  },
);

const formModel = ref<ControlModel>(getFormModel(props.modelValue));

const watchModel = () => {
  return watch(
    formModel,
    ({ exportFrameSize, ...rest }) => {
      emit('update:modelValue', {
        ...rest,

        exportFrameSize: clampSize(
          {
            height: exportFrameSize.height,
            width: exportFrameSize.width,
          },

          props.maxExportFrameSize,
        ),
      });
    },
    { deep: true },
  );
};

let unwatchModel = watchModel();

watch(
  () => props.modelValue,

  (modelValue) => {
    // Prevents infinite update loop
    unwatchModel();

    formModel.value = getFormModel(modelValue);

    unwatchModel = watchModel();
  },

  { deep: true },
);
</script>

<template>
  <form :class="[$style.controls, $style.card]" @submit.prevent="">
    <ElDropdown v-bind="dropdownProps" :class="$style.dropdown">
      <ElButton :class="$style.button" :icon="FilesIcon">Layers</ElButton>

      <template #dropdown>
        <div :class="$style.card">
          <ElCheckboxGroup v-model="formModel.layers" :class="$style['checkbox-container']">
            <ElCheckbox v-for="item in layerItems" :key="item.value" :class="$style.checkbox" :label="item.value">
              {{ item.name }}
            </ElCheckbox>
          </ElCheckboxGroup>
        </div>
      </template>
    </ElDropdown>

    <ElDropdown v-bind="dropdownProps" ref="exportDropdown" :class="$style.dropdown">
      <ElButton data-test="export-menu-trigger" :class="$style.button" :icon="PictureIcon">Export</ElButton>

      <template #dropdown>
        <div :class="$style.card">
          <ElForm novalidate label-position="top" @submit.prevent="emit('export')">
            <ElFormItem label="Height">
              <ElInputNumber
                data-test="export-height"
                v-model="formModel.exportFrameSize.height"
                :min="0"
                :max="maxExportFrameSize.height"
                :step="10"
              />
            </ElFormItem>

            <ElFormItem label="Width">
              <ElInputNumber
                data-test="export-width"
                v-model="formModel.exportFrameSize.width"
                label="Width"
                :min="0"
                :max="maxExportFrameSize.width"
                :step="10"
              />
            </ElFormItem>
            <ElButton data-test="export-button" :class="$style.button" native-type="submit">Export as PNG</ElButton>
          </ElForm>
        </div>
      </template>
    </ElDropdown>

    <ElButton
      data-test="add-marker"
      :class="[$style.button, formModel.isAddingMarker && $style['active-button']]"
      :icon="MapLocationIcon"
      @click="formModel.isAddingMarker = !formModel.isAddingMarker"
    >
      Add Marker
    </ElButton>

    <div :class="$style['history-row']">
      <ElTooltip>
        <template #content>Undo {{ undoTitle }}</template>
        <ElButton
          data-test="undo"
          :class="$style['history-button']"
          :disabled="!undoTitle"
          :icon="ArrowLeftIcon"
          @click="emit('undo')"
        />
      </ElTooltip>

      <ElTooltip>
        <template #content>Redo {{ redoTitle }}</template>
        <ElButton
          data-test="redo"
          :class="$style['history-button']"
          :disabled="!redoTitle"
          :icon="ArrowRightIcon"
          @click="emit('redo')"
        />
      </ElTooltip>
    </div>
  </form>
</template>

<style module lang="scss">
.card {
  background-color: #fff;
  padding: var(--spacing);
  border-radius: calc(var(--spacing) / 2);
}

.card,
.checkbox-container,
.controls {
  display: flex;
  flex-direction: column;
}

.controls {
  gap: var(--spacing);
}

.controls {
  .dropdown {
    display: flex;

    & :global .el-dropdown--default {
      width: 100%;
    }
  }
}

.button {
  width: 100%;
}

.history-row {
  display: flex;
  flex-direction: row;
}

.history-button {
  flex: 1 1 0;
}

.active-button {
  color: var(--el-button-active-text-color);
  border-color: var(--el-button-active-border-color, var(--el-button-active-bg-color));
  background-color: var(--el-button-active-bg-color, var(--el-button-bg-color));
  outline: 0;
}

.checkbox-container .checkbox {
  margin: 0;
}
</style>
