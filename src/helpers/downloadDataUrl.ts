// Pretty hacky, can be swapped out for something better. It's the interface that matters :)
function downloadDataUrl(filename: string, dataUrl: string): void {
  const link = document.createElement('a');

  link.download = filename;
  link.href = dataUrl;

  document.body.appendChild(link);
  link.click();
  link.remove();
}

export default downloadDataUrl;
