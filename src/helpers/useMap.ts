import { Map } from 'mapbox-gl';

const key: InjectionKey<Map> = Symbol('mapbox-map');

function provideMap(map: Map): void {
  provide(key, map);
}

function useMap(): Map {
  const map = inject(key);

  if (!map) {
    throw new Error('Could not inject map');
  }

  return map;
}

export { provideMap, useMap };
