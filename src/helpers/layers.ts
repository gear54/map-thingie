const layerNames = ['poi-labels', 'elevation-shading'] as const;

export type Layer = typeof layerNames[number];
export { layerNames };
