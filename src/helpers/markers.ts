import type { Coordinates } from '@/helpers/geometry';
import { type Icon, iconNames } from '@/helpers/icons';

type BaseMarker = { id: number; coordinates: Coordinates };

type TextMarker = BaseMarker & { type: 'text'; text: string };
type IconMarker = BaseMarker & { type: 'icon'; icon: Icon };

type Marker = TextMarker | IconMarker;

const markerDefaults = { text: 'Sample Text', icon: iconNames[0] };

export { type Marker, markerDefaults };
