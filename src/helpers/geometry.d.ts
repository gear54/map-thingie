type Size = { height: number; width: number };
type Coordinates = { lng: number; lat: number };

export { Size, Coordinates };
