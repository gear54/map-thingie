import type { Size } from './geometry';

function useWindowSize(padding: number): Ref<Size | undefined> {
  const size = ref<{ height: number; width: number }>();

  const setSizeFromWindow = () => {
    size.value = { height: window.innerHeight - padding * 2, width: window.innerWidth - padding * 2 };
  };

  onMounted(() => {
    setSizeFromWindow();
    window.addEventListener('resize', setSizeFromWindow);
  });

  onUnmounted(() => {
    window.removeEventListener('resize', setSizeFromWindow);
  });

  return size;
}

export default useWindowSize;
