import { fileURLToPath, URL } from 'node:url';

import { defineConfig } from 'vite';
import AutoImport from 'unplugin-auto-import/vite';

import vue from '@vitejs/plugin-vue';
import ElementPlus from 'unplugin-element-plus/vite';

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  if (!(command === 'serve' && mode === 'production') && !process.env.VITE_MAPBOX_TOKEN) {
    throw new Error('You must supply VITE_MAPBOX_TOKEN env var when building');
  }
  return {
    plugins: [
      vue(),
      ElementPlus({}),

      AutoImport({
        include: [/\.vue$/, /\.ts$/],
        imports: ['vue'],
      }),
    ],
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url)),
      }
    }
  };
});
