import { test, expect, Page } from '@playwright/test';

const viewport = { height: 1000, width: 1024 };

const addMarker = async (page: Page) => {
  await page.click('data-test=add-marker');
  await page.mouse.click(viewport.width / 2, viewport.height / 2, { delay: 100 });

  const marker = page.locator('data-test=marker-base');

  await marker.waitFor({ state: 'attached' });

  return marker;
};

test.describe('Markers', () => {
  test.beforeEach(async ({ page }) => {
    await page.setViewportSize(viewport);
    await page.goto('/');
  });

  test('Adds marker', async ({ page }) => {
    const marker = await addMarker(page);

    expect(await marker.count()).toBe(1);
  });

  test('Removes marker via delete button', async ({ page }) => {
    const marker = await addMarker(page);

    expect(await marker.count()).toBe(1);

    await page.keyboard.press('Delete');

    expect(await marker.count()).toBe(0);
  });

  test("Doesn't remove marker when editing", async ({ page }) => {
    const marker = await addMarker(page);

    await page.click('data-test=marker-text');
    await page.keyboard.press('Backspace');
    await page.keyboard.press('Delete');

    expect(await marker.count()).toBe(1);
  });
});
