import { test, expect } from '@playwright/test';

const viewport = { height: 1000, width: 1024 };

test.describe('Export as PNG', () => {
  test('Sets correct initial frame size', async ({ page }) => {
    await page.setViewportSize(viewport);
    await page.goto('/');
    const frame = page.locator('data-test=export-frame');

    expect(await frame.count()).toBe(1);
    expect(await frame.boundingBox()).toEqual({ height: 502, width: 514, x: 255, y: 249 });
  });

  test('Resizes frame within window bounds', async ({ page }) => {
    await page.setViewportSize(viewport);
    await page.goto('/');
    await page.click('data-test=export-menu-trigger');
    await page.click('[data-test="export-width"] input', { delay: 100 });
    await page.type('[data-test="export-width"] input', '99999', { delay: 100 });
    await page.click('[data-test="export-height"] input', { delay: 100 });
    await page.type('[data-test="export-height"] input', '99999', { delay: 100 });
    await page.focus('data-test=export-button');

    const frame = page.locator('data-test=export-frame');

    expect(await frame.boundingBox()).toEqual({ height: 1000, width: 1024, x: 0, y: 0 });
  });

  test('Exports PNG file', async ({ page }) => {
    await page.goto('/');
    await page.click('data-test=export-menu-trigger');

    const [download] = await Promise.all([
      // Start waiting for the download
      page.waitForEvent('download'),
      // Perform the action that initiates download
      page.click('data-test=export-button'),
    ]);

    expect(/map-thingie-\d+\.png/.test(download.suggestedFilename())).toBe(true);
  });
});
